import { Component, OnInit } from '@angular/core';
import { Router }              from '@angular/router';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
})
export class Page2Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


goToTechPage(form: any) {
  // if (this.save(form)) {
      // Navigate to the work page
      this.router.navigate(['/page3tech']);
  // }
}

goToActivity(form: any) {
  // if (this.save(form)) {
      // Navigate to the work page
      this.router.navigate(['/page4activity']);
  // }
}

goToPrevious(form: any) {
  // if (this.save(form)) {
      // Navigate to the work page
      this.router.navigate(['/page1']);
  // }
}


}
