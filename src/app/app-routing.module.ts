import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalComponent }    from './personal/personal.component';
import { WorkComponent }        from './work/work.component';
import { AddressComponent }     from './address/address.component';
import { ResultComponent }      from './result/result.component';

import { WorkflowGuard }        from './workflow/workflow-guard.service';
import { WorkflowService }      from './workflow/workflow.service';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3techComponent } from './page3tech/page3tech.component';
import { Page4activityComponent } from './page4activity/page4activity.component';


export const appRoutes: Routes = [
    // 1st Route
    { path: 'page1',  component: Page1Component },
    // { path: 'personal',  component: PersonalComponent },
    // 2nd Route
    { path: 'page2',  component: Page2Component },
    // { path: 'work',  component: WorkComponent, canActivate: [WorkflowGuard] },
    // 3rd Route
    { path: 'page3tech',  component: Page3techComponent },
    // { path: 'address',  component: AddressComponent, canActivate: [WorkflowGuard] },
    // 4th Route
    { path: 'page4activity',  component: Page4activityComponent },
    // { path: 'result',  component: ResultComponent, canActivate: [WorkflowGuard] },
    // 5th Route
    { path: '',   redirectTo: '/page1', pathMatch: 'full' },
    // 6th Route
    { path: '**', component: Page1Component }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true} )],
  exports: [RouterModule],
  providers: [WorkflowGuard]
})

export class AppRoutingModule {}