import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable'

@Component({
  selector: 'app-page3tech',
  templateUrl: './page3tech.component.html',

})
export class Page3techComponent implements OnInit {
   
  inBounds = false;
  edge = {
    top: true,
    bottom: true,
    left: true,
    right: false
  };

  img1 = [{
    posx: 100,
    posy: 100,
    wid: 300,
  }]

  data = [{
    id: 1,
    wid: '50',
    posx: '100',
    posy: '100',

  }, {
    id: 2,
    wid: '70',
    posx: '100',
    posy: '200',
  }, {
    id: 3,
    wid: '95',
    posx: '100',
    posy: '300',
  }, {
    id: 4,
    wid: '99',
    posx: '100',
    posy: '400',
  }];

  constructor() {

  }

 ngOnInit() {
 }

  onStart(event,index:number) {
    // this.img.posx = this.getX(event.style.transform.toString());
    this.data[index].posx = this.getX(event.style.transform.toString());
    this.data[index].posy = this.getY(event.style.transform.toString());
    // alert(this.img.posx )
  }

  onStop(event,index:number) {
    // this.img1[0].posx = this.getX(event.style.transform.toString());
    this.data[index].posx = this.getX(event.style.transform.toString());
    this.data[index].posy = this.getY(event.style.transform.toString());
    // alert(this.img1[0].posx )
  }

  public getX(aData:string) : string
  {
   
    let str = aData.split(',')[0].split('(')[1].toString().trim();
    return str.substring(0,str.length-2);
  }

  public getY(aData:string) : string
  {
    let str = aData.split(',')[1].split(')')[0].toString().trim();
    return str.substring(0,str.length-2);
  }

  checkEdge(event) {
    this.edge = event;
  }
  
}
