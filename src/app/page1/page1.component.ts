import { Component, OnInit } from '@angular/core';
import { Router }              from '@angular/router';

@Component({
  selector: 'page1',
  templateUrl: './page1.component.html',
})
export class Page1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    // this.personal = this.formDataService.getPersonal();
    // console.log('Personal feature loaded!');
}

  goToNext(form: any) {
    // if (this.save(form)) {
        // Navigate to the work page
        this.router.navigate(['/page2']);
    // }
}

}
